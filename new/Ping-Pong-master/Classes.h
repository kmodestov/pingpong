#ifndef _CLASSES_H_
    #define _CLASSES_H_

#include "SDL.h"
#include "SDL_image.h"


class Surface
{

	public:

		Surface();



		static void DrawPixel(SDL_Surface *screen, int x, int y, Uint8 R, Uint8 G, Uint8 B);

};


class Shape
{
	protected:

		int X;

		int Y;

	public:

		Shape();

		Shape(int x, int y);



		int 			GetX();

		int 			GetY();

		virtual void 	DrawShape(SDL_Surface* Surf_Display);

		virtual void 	MoveShape(int x, int y);

		virtual void 	MoveShapeOn(int x, int y);

};


class Rectangle: public Shape
{
	protected:

		int Height;

		int Width;

	public:

		Rectangle();

		Rectangle(int x, int y, int height, int width);


		//int 	GetHeight(); 

		//int 	GetWidth();

		void 	DrawShape(SDL_Surface* Surf_Display);

		void	MoveShape(int x, int y);
		
		void 	MoveShapeOn(int x, int y);


};



class Circle: public Shape
{
	private:

		int Radius;

	public:

		Circle();

		Circle(int x, int y, int radius);




		//int 	GetRadius();

		void 	DrawShape(SDL_Surface* Surf_Display);

		void 	MoveShape(int x, int y);

		void 	MoveShapeOn(int x, int y);

};

class Field
{
	private:

		bool 			Running;

		SDL_Surface* 	Surf_Display; //Main Surface

		Shape* Shapes[3]; 	//0 - The Rectangle Player
							//1 - The Rectangle Opponent
							//2 - The Ball

	public:

		Field();



		int 	OnExecute();

		bool 	OnInit();

		void 	OnEvent(SDL_Event* Event);

		void 	OnLoop(bool* opponent_move, bool* ball_move_x, bool* ball_move_y);

		void 	OnRender();

		void 	OnCleanup();

		void 	OnExit();

	
};


#endif
