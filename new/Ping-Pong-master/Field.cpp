#include "Classes.h"
#include <iostream>

Field::Field()
{
	Surf_Display = NULL;

	Running = true;

	Shapes[0] = new Rectangle(40, 180, 120, 40);
	Shapes[1] = new Rectangle(560, 180, 120, 40);
	Shapes[2] = new Circle(319, 239, 10);
}

int Field::OnExecute()
{
	if(OnInit() == false)
		return -1;

	SDL_Event 	Event;

	int 		ticks, 
				delta_ticks, 
				delay_time;

	bool 		opponent_move = true,
				ball_move_x = true,
				ball_move_y = true;


	while(Running)
	{
		ticks = SDL_GetTicks();

		while(SDL_PollEvent(&Event))
			OnEvent(&Event);

		OnLoop(&opponent_move, &ball_move_x, &ball_move_y);

		OnRender();

		
		delta_ticks = SDL_GetTicks() - ticks;
		delay_time = (100/6) - delta_ticks;

		if(delay_time > 0)
		{
			SDL_Delay(delay_time);
		}
	}

	OnCleanup();

	return 0;
}

bool Field::OnInit()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) < 0)
		return false;

	if((Surf_Display = SDL_SetVideoMode(640, 480, 32, SDL_HWSURFACE | SDL_DOUBLEBUF)) == NULL)
		return false;

	if(SDL_EnableKeyRepeat(1, SDL_DEFAULT_REPEAT_INTERVAL) < 0)
		return false;

	return true;
}

void Field::OnEvent(SDL_Event* Event)
{
	switch(Event->type)
	{
		case SDL_QUIT:
		{
			OnExit();
			break;
		}

		case SDL_KEYDOWN:
		{
			switch(Event->key.keysym.sym)
			{
				case SDLK_UP:
				{
					if(Shapes[0]->GetY() >= 1)
						Shapes[0]->MoveShape(0, -10);
					break;
				}
				case SDLK_DOWN:
				{	
					if(Shapes[0]->GetY() <= 359)
						Shapes[0]->MoveShape(0, 10);
					break;
				}
			}

			break;
		}
	}

}

void Field::OnLoop(bool* opponent_move, bool* ball_move_x, bool* ball_move_y)
{
	/*
	if(*opponent_move)
	{
		if(Shapes[1]->GetY() >= 1)
			Shapes[1]->MoveShape(0, -2);
		else
			*opponent_move = false;
	}
	else
	{
		if(Shapes[1]->GetY() <= 359)
			Shapes[1]->MoveShape(0, 2);
		else
			*opponent_move = true;
	}
	*/
	if(Shapes[2]->GetY() < 359 && Shapes[2]->GetY() > 1)
		Shapes[1]->MoveShapeOn(Shapes[1]->GetX(), Shapes[2]->GetY());


	if(*ball_move_x)
	{
		if(Shapes[2]->GetX() < Shapes[1]->GetX())
			Shapes[2]->MoveShape(3, 0);
		else if(Shapes[2]->GetX() >= Shapes[1]->GetX() && Shapes[2]->GetX() <= (Shapes[1]->GetX() + 10))
		{
			if(Shapes[2]->GetY() >= Shapes[1]->GetY() && Shapes[2]->GetY() <= (Shapes[1]->GetY() + 120))
				*ball_move_x = false;
			else
				Shapes[2]->MoveShapeOn(319, 239);
		}
		else
			Shapes[2]->MoveShapeOn(319, 239);
	}
	else
	{
		if(Shapes[2]->GetX() > (Shapes[0]->GetX() + 40))
			Shapes[2]->MoveShape(-3, 0);
		else if(Shapes[2]->GetX() <= (Shapes[0]->GetX() + 40) && Shapes[2]->GetX() >= (Shapes[0]->GetX() + 30))
		{
			if(Shapes[2]->GetY() >= Shapes[0]->GetY() && Shapes[2]->GetY() <= (Shapes[0]->GetY() + 120))
				*ball_move_x = true;
			else
				Shapes[2]->MoveShapeOn(319, 239);
		}
		else
			Shapes[2]->MoveShapeOn(319, 239);
	}

	if(*ball_move_y)
	{
		if(Shapes[2]->GetY() >= 15)
			Shapes[2]->MoveShape(0, -3);
		else
			*ball_move_y = false;
	}
	else
	{
		if(Shapes[2]->GetY() <= 465)
			Shapes[2]->MoveShape(0, 3);
		else
			*ball_move_y = true;
	}

}

void Field::OnRender()
{
	for(int i = 0; i < 480; i++)
		for(int j = 0; j < 640; j++)
			Surface::DrawPixel(Surf_Display, j, i, 255, 255, 255);

	for(int i = 0; i < 3; i++)
		Shapes[i]->DrawShape(Surf_Display);

	SDL_Flip(Surf_Display);
}

void Field::OnExit()
{
	Running = false;
}

void Field::OnCleanup()
{
	SDL_FreeSurface(Surf_Display);

	SDL_Quit();
}
