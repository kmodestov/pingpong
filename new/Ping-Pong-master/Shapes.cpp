#include "Classes.h"
#include <math.h>

const double PI  = 3.141592653589793238462;




Shape::Shape(): X(0), Y(0)
{	}

Shape::Shape(int x, int y): X(x), Y(y)
{	}

int Shape::GetX()
{
	return X;
}

int Shape::GetY()
{
	return Y;
}

void Shape::DrawShape(SDL_Surface* Surf_Display)
{

}

void Shape::MoveShape(int x, int y)
{

}

void Shape::MoveShapeOn(int x, int y)
{

}



Rectangle::Rectangle(): Shape(), Height(0), Width(0)
{	}

Rectangle::Rectangle(int x, int y, int height, int width): Shape(x, y), Height(height), Width(width) 
{	}

void Rectangle::DrawShape(SDL_Surface* Surf_Display)
{
	for(int i = Y; i < Y + Height; i++)
		for(int j = X; j < X + Width; j++)
			Surface::DrawPixel(Surf_Display, j, i, 0, 255, 0);
}

void Rectangle::MoveShape(int x, int y)
{
	X += x;

	Y += y;
}

void Rectangle::MoveShapeOn(int x, int y)
{
	X = x;

	Y = y;
}





Circle::Circle(): Shape(), Radius(0)
{	}

Circle::Circle(int x, int y, int radius): Shape(x, y), Radius(radius)
{	}

void Circle::DrawShape(SDL_Surface* Surf_Display)
{
	for(int i = 0; i < 360; i++)
		Surface::DrawPixel(Surf_Display, X + Radius * cos(i*PI/180), Y + Radius * sin(i*PI/180), 0, 0, 255);
}

void Circle::MoveShape(int x, int y)
{
	X += x;

	Y += y;
}

void Circle::MoveShapeOn(int x, int y)
{
	X = x;

	Y = y;
}